package es.MAQUILES;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class App extends Application{

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		primaryStage.setTitle("Drag & Drop");
		
        try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(App.class.getResource("Actividad5.fxml"));
			
			GridPane escena = loader.load();
			primaryStage.setScene(new Scene(escena));
			primaryStage.show();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		
		launch(args);
		
	}

}
