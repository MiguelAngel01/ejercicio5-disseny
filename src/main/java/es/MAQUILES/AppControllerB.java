package es.MAQUILES;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class AppControllerB implements Initializable{

	@FXML
	private Text resultado;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		resultado.setText("El número de movimientos que has necesitado son: " + AppController.moves);
		
	}
	
	@FXML
	private void cerrar(ActionEvent event) {
		
		Node source = (Node) event.getSource();
		Stage stage = (Stage) source.getScene().getWindow();
	    stage.close();
		
	}
	
}
