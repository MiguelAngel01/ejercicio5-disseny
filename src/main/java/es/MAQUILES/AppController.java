package es.MAQUILES;

import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class AppController implements Initializable{

	@FXML
	private ImageView jokerImg;
	@FXML
	private ImageView queenImg;
	@FXML
	private ImageView crowImg;
	@FXML
	private ImageView violetImg;
	@FXML
	private ImageView queen;
	@FXML
	private ImageView crow;
	@FXML
	private ImageView joker;
	@FXML
	private ImageView violet;
	@FXML
	private ImageView queenCheck;
	@FXML
	private ImageView crowCheck;
	@FXML
	private ImageView jokerCheck;
	@FXML
	private ImageView violetCheck;
	
	public static int moves = 0;
	private boolean queenCorrect = false;
	private boolean crowCorrect = false;
	private boolean jokerCorrect = false;
	private boolean violetCorrect = false;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {}
	
	@FXML
	private void handleDragOver(DragEvent event) {
		
		event.acceptTransferModes(TransferMode.ANY);
		
		event.consume();
			
	}
	
	@FXML
	private void handleDragEntered1(DragEvent event) throws FileNotFoundException {
		
		handleDragEnteredGlobal(event, queen);
		correctImage(queen);
	}
	
	@FXML
	private void handleDragEntered2(DragEvent event) throws FileNotFoundException {
		
		handleDragEnteredGlobal(event, crow);
		correctImage(crow);
		
	}
	
	@FXML
	private void handleDragEntered3(DragEvent event) throws FileNotFoundException {
		
		handleDragEnteredGlobal(event, joker);
		correctImage(joker);
		
	}
	
	@FXML
	private void handleDragEntered4(DragEvent event) throws FileNotFoundException {
		
		handleDragEnteredGlobal(event, violet);
		correctImage(violet);
		
	}
	
	private void handleDragEnteredGlobal(DragEvent event, ImageView image) {
		
		if (event.getGestureSource() != image &&
                event.getDragboard().hasString())
        {
			image.setImage(new Image(event.getDragboard().getString()));
        }
        event.consume();
		
	}

	@FXML
	private void setOnDragDetected1(MouseEvent event) {
		
		setOnDragDetectedGlobal(event, queen);
	}
	
	@FXML
	private void setOnDragDetected2(MouseEvent event) {
		
		setOnDragDetectedGlobal(event, crow);
		
	}
	
	@FXML
	private void setOnDragDetected3(MouseEvent event) {
		
		setOnDragDetectedGlobal(event, joker);
		
	}
	
	@FXML
	private void setOnDragDetected4(MouseEvent event) {
		
		setOnDragDetectedGlobal(event, violet);
		
	}
	
	@FXML
	private void setOnDragDetectedJoker(MouseEvent event) {
		
		setOnDragDetectedGlobal(event, jokerImg);
		
	}
	
	@FXML
	private void setOnDragDetectedQueen(MouseEvent event) {
		
		setOnDragDetectedGlobal(event, queenImg);
		
	}
	
	@FXML
	private void setOnDragDetectedViolet(MouseEvent event) {
		
		setOnDragDetectedGlobal(event, violetImg);
		
	}
	
	@FXML
	private void setOnDragDetectedCrow(MouseEvent event) {
		
		setOnDragDetectedGlobal(event, crowImg);
		
	}
	
	private void setOnDragDetectedGlobal(MouseEvent event, ImageView image) {
		
		Dragboard db = image.startDragAndDrop(TransferMode.ANY);
		
		ClipboardContent cb = new ClipboardContent();
		cb.putString(image.getImage().getUrl());
		
		db.setContent(cb);
		
		moves++;
		
		image.setImage(new Image("?.png"));
		
		event.consume();
		
	}
	
	private void correctImage(ImageView image) throws FileNotFoundException {
		
		String[] filePath = image.getImage().getUrl().split("/");
		String file = filePath[filePath.length - 1];
		
		boolean correct = (file.equalsIgnoreCase(image.getId() + ".jpg"));
		
		Image fCorrect = new Image("correct.png");
		Image fIncorrect = new Image("incorrect.jpg");
		
		switch (image.getId()) {
		case "queen":
			if (correct) {
				queenCheck.setImage(fCorrect);
				queenCorrect = true;
			} else {
				queenCheck.setImage(fIncorrect);
				queenCorrect = false;
			}
			break;
		case "crow":
			if (correct) {
				crowCheck.setImage(fCorrect);
				crowCorrect = true;
			} else {
				crowCheck.setImage(fIncorrect);
				crowCorrect = false;
			}
			break;
		case "joker":
			if (correct) {
				jokerCheck.setImage(fCorrect);
				jokerCorrect = true;
			} else {
				jokerCheck.setImage(fIncorrect);
				jokerCorrect = false;
			}
			break;
		case "violet":
			if (correct) {
				violetCheck.setImage(fCorrect);
				violetCorrect = true;
			} else {
				violetCheck.setImage(fIncorrect);
				violetCorrect = false;
			}
			break;
		default:
			break;
		}
		
		if (queenCorrect && crowCorrect && jokerCorrect && violetCorrect) {
			finalizar();
		}
		
	}
	
	private void finalizar() {
		
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(App.class.getResource("Actividad5b.fxml"));
			
			AnchorPane escena = loader.load();
			
            Stage stage = new Stage();
            stage.setScene(new Scene(escena));
            stage.show();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
