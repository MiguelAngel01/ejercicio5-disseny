module es.MAQUILES {
    requires javafx.controls;
    requires javafx.fxml;
	requires javafx.graphics;
	requires javafx.base;

    opens es.MAQUILES to javafx.fxml;
    exports es.MAQUILES;
}